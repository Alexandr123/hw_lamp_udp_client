from Crypto.Cipher import AES
import struct
import base64

def stringToBase64(s):
    return base64.b64encode(s.encode('utf-8'))

def base64ToString(b):
    return base64.b64decode(b).decode('utf-8')

def convert_string_to_bytes(string):
    bytes = b''
    for i in string:
        bytes += struct.pack("B", ord(i))
    return bytes   
    
key = '\x15\x2B\x7E\x16\x28\xAE\xD2\xA6\xAB\xF7\x15\x88\x09\xCF\x4F\x3C'
vi =  '\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'
key = convert_string_to_bytes(key)
vi = convert_string_to_bytes(vi)
cipher = AES.new(key, AES.MODE_CBC, vi)


inp = 'cwsbLnDa2xxzibQs9ja09feciU7sxZqSYdFQAQb1D08='
print("INPUT: " + str(inp))
inp = base64.b64decode(inp)
print("FROM base64: " + str(inp))
inp =cipher.decrypt(inp)
print("FROM AES: " + str(inp))
inp = base64.b64decode(inp)
print("FROM base64: " + str(inp.decode()))
#print("AES: " + str(msg))#.encode("hex")
