import socket
import base64
import struct
from time import sleep
from Crypto.Cipher import AES

def stringToBase64(s):
    return base64.b64encode(s.encode('utf-8'))

def base64ToString(b):
    return base64.b64decode(b).decode('utf-8')
    
def convert_string_to_bytes(string):
    bytes = b''
    for i in string:
        bytes += struct.pack("B", ord(i))
    return bytes 

def calc_base64_length(str_len):
	return int((4 * str_len / 3) + 3) & ~3
	      
def cmd_to_sixteen(string):
	while True:
		if calc_base64_length(len(string))%16==0:
			return string
		else:
			string+=" "
def base_to_sixteen(base):
	while True:
		if len(base)%16==0:
			return base
		else:
			base+=b'='

#!!!!!!!!!!string = string[:-1] Убрать последний символ из строки 
#PING:LAMP:LAMP:A0;20;A6;10;57;E5:192.168.85.27:1  	BAD
#GETS:LAMP:18:200:200:200:1:255:255:255				GOOD
#GETS:LAMP:18:200:200:200:1:255:255:25				GOOD

key = '12345678\x00\x00\x00\x00\x00\x00\x00\x00'#'\x15\x2B\x7E\x16\x28\xAE\xD2\xA6\xAB\xF7\x15\x88\x09\xCF\x4F\x3C'#\x00
vi =  '\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'
key = convert_string_to_bytes(key)
vi = convert_string_to_bytes(vi)
	
command = "CONF:Lamp:18:255:255:100:1:255:000:000"#"PING"#26 50
serverAddressPort   = ('192.168.85.255', 8888)
bufferSize          = 1024
# Create a UDP socket at client side
UDPClientSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
UDPClientSocket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
# Send to server using created UDP socket


def encrypt(cmd):
	global key
	global vi
	cipher = AES.new(key, AES.MODE_CBC, vi)
	#cmd = cmd_to_sixteen(cmd)
	cmd = base64.b64encode(cmd.encode())
	cmd = cipher.encrypt(base_to_sixteen(cmd))
	cmd = base64.b64encode(cmd)
	return cmd
	
def decrypt(resp):
	global key
	global vi
	cipher = AES.new(key, AES.MODE_CBC, vi)
	print("RAW: " + str(resp.decode('utf-8')))#.decode('utf-8')
	resp = base64.b64decode(resp.decode('utf-8'))#.decode('utf-8')
	print("FROM BASE64: " + str(resp))
	
	resp = cipher.decrypt(resp)
	print(len(resp))
	print("FROM AES: " + str(resp))

	
	
	
	resp = base64.b64decode(resp)
	return resp.decode('utf-8')


def lampa_test():
	print(encrypt(command))
	UDPClientSocket.sendto(command.encode(), serverAddressPort)#encrypt(
	msgFromServer = UDPClientSocket.recvfrom(bufferSize)
	response = decrypt(msgFromServer[0])
	print("Response: " + str(response))
	
def lampa_testo(scale):
	cmd = "CONF:Lamp:2:255:255:"+str(scale)+":1:255:255:0"
	print(encrypt(command))
	UDPClientSocket.sendto(command.encode(), serverAddressPort)#encrypt(
	#msgFromServer = UDPClientSocket.recvfrom(bufferSize)
	#response = decrypt(msgFromServer[0])
	#print("Response: " + str(response))

#UDPClientSocket.bind(serverAddressPort)
def monitor():
	#UDPClientSocket.bind(serverAddressPort)
	msgFromServer = UDPClientSocket.recvfrom(bufferSize)
	print(msgFromServer[0])
	print(msgFromServer[1])
	UDPClientSocket.sendto(msgFromServer[0], serverAddressPort)
	sleep(5)
	UDPClientSocket.sendto("ХУЙ ПИЗДА ДЖИГУРДА".encode(), serverAddressPort)
	sleep(5)

#i = 0
#while True:
#	lampa_testo(i)
	#i+=1
	#sleep(2)
lampa_test()
#while True:
#	monitor()   
#data = "2IlsOujn7qTv0Z4+SwIMnKAuXrA6ZgHOElK8aM0met5FNzTW3qIrIdwdWrReazrX52XFUckSA1p8qG3RrGt10fqpkmVTjcHXG4M/NFs7cEM="
#response = decrypt(data)
#print("Response: " + str(response))
